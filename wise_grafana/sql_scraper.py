from prometheus_client import start_http_server, Gauge, Gauge
import random
import time
import mysql.connector
import signal
import sys
from threading import Thread
def sigterm_handler(_signo, _stack_frame):
    # Raises SystemExit(0):
    sys.exit(0)

signal.signal(signal.SIGTERM, sigterm_handler)


class sqlScraper(Thread):
    class config:
        def __init__(self):
            self.scrape_delay = 1

    def __init__(self):
        super(sqlScraper, self).__init__()
        self.my_gpu_active_c = Gauge("ActiveGpuG","Counter of active gpus")
        self.my_running_camera_c = Gauge("RunningCameraG","Running Cameras")
        self.my_stopped_camera_c = Gauge("StoppedCameraG","Stopped Cameras")
        self.my_failed_camera_c = Gauge("FailedCameraG","Fail Cameras")
        self.my_total_camera_c = Gauge("TotalCameraG","Total Cameras")
        self.my_camera_delay_g = Gauge("CamerasDelayG","Total Delays",labelnames=["camera","detection"])
        self.mydb = mysql.connector.connect(
                      host="localhost",
                      user="root",
                      password="viisights5344",
                      database="discovery"
                    )
        self.mycursor = self.mydb.cursor()

    def get_from_sql(self, sql_query):
        my_cursor = self.mydb.cursor()
        my_cursor.execute(sql_query)
        my_result = my_cursor.fetchall()
        return my_result

    def update_counter_with_value(self, counter, sql_query):
        my_cursor = self.mydb.cursor()
        my_cursor.execute(sql_query)
        my_result = my_cursor.fetchall()
        counter.set(my_result[0][0])
        print(str(counter.describe()) + str(my_result[0][0]))

    def update_counter_with_len(self, counter, sql_query):
        my_db = mysql.connector.connect(
            host="localhost",
            user="root",
            password="viisights5344",
            database="discovery"
        )
        my_cursor = my_db.cursor()
        my_cursor.execute(sql_query)
        my_result = my_cursor.fetchall()
        counter.set(len(my_result))
        #print(str(counter.describe()) + str(len(my_result)))

    def monitor_delays(self):
        running_cams = self.get_from_sql("select ext_id from content where (status like '%INPROGRESS%')")
        running_cams_list = list()
        for item in running_cams:
            running_cams_list.append(item[0])
        delay_table = self.get_from_sql("select time_stamp, m.name, d.resource_id, avg(val) from metrics m join metrics_data d on m.seq_id=d.metric_id  where time_stamp<= DATE_SUB( now(), INTERVAL 1 MINUTE ) group by name, resource_id;")
        for item in delay_table:
            delay_type = item[1]
            cam_name = item[2]
            delay = item[3]
            if running_cams_list.count(cam_name) > 0:
                #if delay > 3.0:
                print(str(cam_name) + " " + delay_type + " " + str(round(delay,2)))
                self.my_camera_delay_g.labels(camera=cam_name, detection=delay_type).set(round(delay,2))
            else:
                self.my_camera_delay_g.labels(camera=cam_name, detection=delay_type).set(round(0, 2))

        #print(delay_table)

    def monitor_cam_status(self):
        self.update_counter_with_len(self.my_total_camera_c, "select ext_id from content ")
        self.update_counter_with_len(self.my_failed_camera_c,
                                     "select ext_id from content where (status like '%FAILED%') ")
        self.update_counter_with_len(self.my_running_camera_c,
                                 "select ext_id from content where (status like '%INPROGRESS%') ")

    def run(self):
        while True:
            time.sleep(1)
            self.update_counter_with_value(self.my_gpu_active_c, "select count(*) from gpu_device where active=0")
            self.monitor_cam_status()
            self.monitor_delays()

if __name__ == '__main__':
    # Start up the server to expose the metrics.
    try:
        wise_scraper = sqlScraper()
        print("Starting http on port 5000")
        #prom_server = start_http_server(5000)
        # Generate some requests.
        while True:
            time.sleep(1)
            wise_scraper.run()
    except OSError:
        print(OSError)
    finally:
        prom_server = None

