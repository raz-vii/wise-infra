import time
import redis
from prometheus_client import start_http_server, Gauge, Counter, Summary
from threading import Thread
import mysql.connector
import sys


class sqlScraper(Thread):
    class config:
        def __init__(self):
            self.scrape_delay = 1

    def __init__(self):
        super(sqlScraper, self).__init__()
        self.my_gpu_active_c = Gauge("ActiveGpuG","Counter of active gpus")
        self.my_running_camera_c = Gauge("RunningCameraG","Running Cameras")
        self.my_stopped_camera_c = Gauge("StoppedCameraG","Stopped Cameras")
        self.my_failed_camera_c = Gauge("FailedCameraG","Fail Cameras")
        self.my_total_camera_c = Gauge("TotalCameraG","Total Cameras")
        self.my_camera_delay_g = Gauge("CamerasDelayG","Total Delays",labelnames=["camera","detection"])
        self.mydb = mysql.connector.connect(
                      host="localhost",
                      user="root",
                      password="viisights5344",
                      database="discovery"
                    )
        self.mycursor = self.mydb.cursor()

    def get_from_sql(self, sql_query):
        my_cursor = self.mydb.cursor()
        my_cursor.execute(sql_query)
        my_result = my_cursor.fetchall()
        return my_result

    def update_counter_with_value(self, counter, sql_query):
        my_cursor = self.mydb.cursor()
        my_cursor.execute(sql_query)
        my_result = my_cursor.fetchall()
        counter.set(my_result[0][0])
        print(str(counter.describe()) + str(my_result[0][0]))

    def update_counter_with_len(self, counter, sql_query):
        my_db = mysql.connector.connect(
            host="localhost",
            user="root",
            password="viisights5344",
            database="discovery"
        )
        my_cursor = my_db.cursor()
        my_cursor.execute(sql_query)
        my_result = my_cursor.fetchall()
        counter.set(len(my_result))
        #print(str(counter.describe()) + str(len(my_result)))

    def monitor_delays(self):
        running_cams = self.get_from_sql("select ext_id from content where (status like '%INPROGRESS%')")
        running_cams_list = list()
        for item in running_cams:
            running_cams_list.append(item[0])
        delay_table = self.get_from_sql("select time_stamp, m.name, d.resource_id, avg(val) from metrics m join metrics_data d on m.seq_id=d.metric_id  where time_stamp<= DATE_SUB( now(), INTERVAL 1 MINUTE ) group by name, resource_id;")
        for item in delay_table:
            delay_type = item[1]
            cam_name = item[2]
            delay = item[3]
            if running_cams_list.count(cam_name) > 0:
                #if delay > 3.0:
                print(str(cam_name) + " " + delay_type + " " + str(round(delay,2)))
                self.my_camera_delay_g.labels(camera=cam_name, detection=delay_type).set(round(delay,2))
            else:
                self.my_camera_delay_g.labels(camera=cam_name, detection=delay_type).set(round(0, 2))

        #print(delay_table)

    def monitor_cam_status(self):
        self.update_counter_with_len(self.my_total_camera_c, "select ext_id from content ")
        self.update_counter_with_len(self.my_failed_camera_c,
                                     "select ext_id from content where (status like '%FAILED%') ")
        self.update_counter_with_len(self.my_running_camera_c,
                                 "select ext_id from content where (status like '%INPROGRESS%') ")

    def run(self):
        while True:
            time.sleep(1)
            #self.update_counter_with_value(self.my_gpu_active_c, "select count(*) from gpu_device where active=0")
            self.monitor_cam_status()
            self.monitor_delays()

class RedisExporterServer(Thread):

    def __init__(self,channel, port=5000):
        super(RedisExporterServer, self).__init__()
        self.redis_exporter_keep_alive_s = Summary("wise_exporter_counter","Keep alive counter of wise")
        self.channal = channel
        try:
            start_http_server(port)
        except:
            print("Redis exporter port is already binded:" + str(port))
            sys.exit(1)
        self.redis = redis.Redis(unix_socket_path='/var/run/redis/redis.sock')
        self.redis_channel = self.redis.pubsub()
        self.redis_channel.subscribe("grafana-gauge")
        self.redis_exporter_keep_alive = Counter("redis_exporter_keep_alive", 'RedisExporter keep alive')
        self.redis_exporter_metric = Gauge("wise_gauge", 'Wise metric', ['cam', 'module', 'label1', 'label2'])
        self.redis_exporter_metric.labels(cam='cam', module='module', label1='label1', label2='label2').inc()

    def post_metric(self, message):
        try:
            m = str(message)[2:-1]
            #print(m)
            tokens = str(m).split(',')
            print(tokens)
            if len(tokens) >= 5:
                self.redis_exporter_metric.labels(cam=tokens[0], module=tokens[1], label1=tokens[2],
                                                  label2=tokens[3]).set(round(float(tokens[4]),2))
        except:
            print('parse failed:' + message)

    def run(self):
        print('RedisExporter start')
        prev_sec = 0
        while True:
            self.redis_exporter_keep_alive.inc()
            response = self.redis_channel.get_message(timeout=1)
            self.redis_exporter_keep_alive_s.observe(1)
            if response is not None:
                if response['type'] == 'message':
                    self.post_metric(response['data'])
                    if time.localtime().tm_sec != prev_sec:
                        prev_sec =  time.localtime().tm_sec
                        print(response['data'])




class RedisExporterMetric:
    def __init__(self, cam_id = 0, module_name = 'wise', label1 = 'label1', label2 = 'lable2'):
        self.cam_id = cam_id
        self.module_name = module_name
        self.label1 = label1
        self.label2 = label2
        self.msg_params =  list()
        self.msg_params.append(str(self.cam_id))
        self.msg_params.append(self.module_name)
        self.msg_params.append(self.label1)
        self.msg_params.append(self.label2 )
        self.msg_prefix = ','.join(self.msg_params)
        self.msg_prefix += ','
        self.redis = redis.Redis(unix_socket_path='/var/run/redis/redis.sock')

    def build_message(self, val):
        return self.msg_prefix + str(val)

    def set(self, value):
        self.redis.publish("grafana-summary",self.build_message(value))


class RedisExporterMetricSummary(RedisExporterMetric):
    def __init__(self, cam_id = 0, module_name = 'wise', label1 = 'label1', label2 = 'lable2'):
        super(RedisExporterServer, self).__init__(cam_id , module_name , label1 , label2)

    def set(self, value):
        self.redis.publish("grafana-summary",self.build_message(value))

class testThread(Thread):
    def run(self):
        print('testThread start')

        my_metric = RedisExporterMetric(0, 'wise-test', 'wisel1', 'wisel2')
        my_metricSummary = RedisExporterMetricSummary(0, 'wise-test', 'wisel1', 'wisel2')
        tries = 0
        self.redis = redis.Redis(unix_socket_path='/var/run/redis/redis.sock')
        while tries < 60:
            tries += 1
            time.sleep(1)
            print(str(tries))
            my_metric.set(4.99)
            my_metricSummary.set(1)

if __name__ == "__main__":
    try:
        redis_exporter = RedisExporterServer(5000)
        sql_scraper = sqlScraper()
        redis_exporter.start()
        sql_scraper.start()
        #test_tread = testThread()
        #test_tread.start()
    except KeyboardInterrupt:
        pass
    finally:
        sys.exit(0)




